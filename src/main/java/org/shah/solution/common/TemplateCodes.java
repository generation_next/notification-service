package org.shah.solution.common;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("")
public class TemplateCodes {
	private final Map<String, String> template = new HashMap<String, String>();

	public Map<String, String> getTemplate() {
		return template;
	}

}

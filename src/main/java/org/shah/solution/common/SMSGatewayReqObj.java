package org.shah.solution.common;

public class SMSGatewayReqObj {
	private String deviceId;
	private String phoneNumber;
	private String message;

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "[{\"device_id\":"+"\""+ deviceId +"\",\"phone_number\":\""+ phoneNumber + "\",\"message\":\""+ message+"\"}]";
	}
}

package org.shah.solution.common;

import java.io.Serializable;

public class NotificationData implements Serializable{
	
	private static final long serialVersionUID = 809885296888106853L;
	private String subject;
	private String title;
	private String content;
	private String body;
	private String message;
	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Subject :" + this.subject + "\n"+"Content: "+this.content+"\n"+"Title :"+ this.title;
		
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}

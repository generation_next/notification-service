package org.shah.solution.common;

public interface CommonConstants {
	public String PARAM_EMAIL = "email";
	public String PARAM_PWD = "password";
	public String API_KEY="Authorization";
	public String PARAM_DEVICE = "device_id";
	public String PARAM_CONTACT = "phone_number";
	public String PARAM_MSG = "message";
	public static final String _EMAIL ="EMAIL";
	public static final String _MOBILE ="MOB";
	
}

package org.shah.solution.common;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("app")
public class NotificationsContent {

	private final Map<String, NotificationData> notification = new HashMap<String, NotificationData>();
	
	public Map<String, NotificationData> getNotification() {
		return notification;
	}
}

package org.shah.solution.service;

import java.util.Map;

import org.shah.solution.common.NotificationData;
import org.shah.solution.common.SMSGatewayReqObj;
import org.shah.solution.config.GatewayConfig;
import org.shah.solution.model.SmsGatewayConfigObj;
import org.shah.solution.notification.model.Notification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;

@Service
public class SMSService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SMSService.class);
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private GatewayConfig gatewayConfig;
	

	@Value("${config.sms.gateway.uri}")
	private String uri;
	

	public void sendSMSMsg(Notification notification, NotificationData data) throws JsonProcessingException {
		String message = data.getMessage();
		String code = message.replaceAll("\\$code\\$", notification.getMessage());
		Map<String, SmsGatewayConfigObj> listConfig = gatewayConfig.getGatewayMap();
		if (listConfig != null && !listConfig.isEmpty()) {
			SmsGatewayConfigObj config = listConfig.get(notification.getUserCountry());
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.add(HttpHeaders.AUTHORIZATION,config.getApiKey());
			SMSGatewayReqObj jsonRequest = new SMSGatewayReqObj(); 
			jsonRequest.setDeviceId(config.getDevice());
			jsonRequest.setPhoneNumber((notification.getUserCountryCode()!=null?notification.getUserCountryCode():("").trim()) + notification.getUserPhone());
			jsonRequest.setMessage(code);
			System.out.println(jsonRequest);
			LOGGER.info("JSON OBJECT :=> \n"+ jsonRequest.toString());
			HttpEntity<String> request = new HttpEntity<String>(jsonRequest.toString(),headers);
			String result = restTemplate.postForObject(uri.trim(), request, String.class);
			System.out.println(result);
		}
		
	}
	
}

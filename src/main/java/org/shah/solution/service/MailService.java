package org.shah.solution.service;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.shah.solution.common.NotificationData;
import org.shah.solution.notification.model.Notification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

@Service
public class MailService {

	private static final Logger logger = LoggerFactory.getLogger(MailService.class);

	@Autowired
	private JavaMailSender javaMailService;

	@Autowired
	private Configuration freemarkerConfiguration;

	@Value("${config.support.email.from}")
	private String emailFrom;
	

	private static final String DEFAULT_ENCODING = "utf-8";

	public SimpleMailMessage constructEmail(String subject, String body, String emailTo) {
		SimpleMailMessage email = new SimpleMailMessage();
		email.setSubject(subject);
		email.setText(body);
		email.setTo(emailTo);
		email.setFrom(emailFrom);
		return email;
	}

	public void sendHtmlMessage(Notification message, NotificationData supportData, String templateName) {
		try {
			MimeMessage msg = javaMailService.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(msg, true, DEFAULT_ENCODING);
			InternetAddress recipientAddress = new InternetAddress(message.getUserEmail());
			// InputStreamSource imageSource1 = new
			// ByteArrayResource(IOUtils.toByteArray(getClass().getResourceAsStream("/images/OTP.jpg")));
			helper.setFrom(emailFrom);
			helper.setTo(recipientAddress);
			helper.setSubject(supportData.getSubject());
			String content = generateContent(message, supportData, templateName);
			helper.setText(content, true);
			helper.addInline("LOGO", new ClassPathResource("images/logo.jpg"));
			helper.addInline("OTPJPG", new ClassPathResource("images/OTP.jpg"));
			helper.addInline("MAILTO", new ClassPathResource("images/mailto.jpg"));
			helper.addInline("SPACER", new ClassPathResource("images/spacer.png"));
			javaMailService.send(msg);
		} catch (MessagingException e) {
			logger.error("build email failed", e);
		} catch (Exception e) {
			logger.error("send email failed", e);
		}
	}

	public String generateContent(Notification message, NotificationData supportData, String templateName) throws MessagingException {

		try {

			Map<String, String> data = new HashMap<>();
			data.put("OTP", message.getMessage());
			data.put("name", message.getUserName());
			data.put("mailfrom", emailFrom);
			Map<String, Map<String, String>> context = Collections.singletonMap("data", data);
			Template template = freemarkerConfiguration.getTemplate(templateName.trim()+".ftl", DEFAULT_ENCODING);
			Map<String, Object> sharedVariables = new HashMap<>();
			sharedVariables.put("content", supportData.getContent());
			sharedVariables.put("title", supportData.getTitle());
			sharedVariables.put("body", supportData.getBody());
			freemarkerConfiguration.setSharedVariable("sharedData", sharedVariables);
			return FreeMarkerTemplateUtils.processTemplateIntoString(template, context);
		} catch (IOException e) {
			logger.error("FreeMarker template not exist", e);
			throw new MessagingException("FreeMarker template not exist", e);
		} catch (TemplateException e) {
			logger.error("FreeMarker process failed", e);
			throw new MessagingException("FreeMarker process failed", e);
		}
	}

}

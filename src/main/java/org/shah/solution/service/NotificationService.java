package org.shah.solution.service;

import java.io.StringReader;
import java.util.Map;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import org.shah.solution.common.CommonConstants;
import org.shah.solution.common.NotificationData;
import org.shah.solution.common.NotificationsContent;
import org.shah.solution.common.TemplateCodes;
import org.shah.solution.notification.model.Notification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;

@Service
public class NotificationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationService.class);
	
	@Value("${jsa.activemq.from}")
	private String fromMsg;

	@Autowired
	private JAXBContext jaxbContext;

	@Autowired
	private SMSService smsService;
	
	@Autowired
	private MailService mailService;
	
	@Autowired
	private TemplateCodes templateCodes;
	
	@Autowired
	private NotificationsContent notificationsContent;

    public void sendMessage(Notification notification) {
    	String channel = notification.getMessageType();
    	Map<String, String> templates =  templateCodes.getTemplate();
    	String messageType =  templates.get(notification.getMessageCode().trim());
    	Map<String, NotificationData> content=  notificationsContent.getNotification();
    	LOGGER.debug("Template content \n"+ content.toString());
    	NotificationData notificationData = content.get(messageType.trim());
    	LOGGER.debug("Notification data = \n" + notificationData);
    	switch (channel.toUpperCase()) {
		case CommonConstants._EMAIL:
			mailService.sendHtmlMessage(notification, notificationData, messageType);
			break;
		case CommonConstants._MOBILE:
			try {
				smsService.sendSMSMsg(notification, notificationData);
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;	
		default:
			break;
		}
		
	}
 /*
	public String sendResetCode(UserCredentials user, String msg) {
		String code = resetMsg.replaceAll("\\$code\\$", msg);
		HttpMessageConverter<?> formHttpMessageConverter = new FormHttpMessageConverter();
		HttpMessageConverter<?> stringHttpMessageConverternew = new StringHttpMessageConverter();
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(formHttpMessageConverter);
		messageConverters.add(stringHttpMessageConverternew);
		restTemplate.setMessageConverters(messageConverters);
		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		Map<String, SmsGatewayConfigObj> listConfig = gatewayConfig.getGatewayMap();
		SmsGatewayConfigObj config = listConfig.get(user.getCountrycode());
		map.add(CommonConstants.PARAM_EMAIL, config.getEmail());
		map.add(CommonConstants.PARAM_PWD, config.getPassword());
		map.add(CommonConstants.PARAM_DEVICE, config.getDevice());
		map.add(CommonConstants.PARAM_CONTACT, "+" + user.getCountrycode() + user.getContact());
		map.add(CommonConstants.PARAM_MSG, setMsgTemplate(code));
		String result = restTemplate.postForObject(uri, map, String.class);
		return result;
	}*/

	public Notification unmarshal(String message) throws JAXBException, XMLStreamException, FactoryConfigurationError {
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		XMLStreamReader reader = XMLInputFactory.newInstance().createXMLStreamReader(new StringReader(message));
		Notification notification = (Notification) unmarshaller.unmarshal(reader);
		return notification;
	}


	
}

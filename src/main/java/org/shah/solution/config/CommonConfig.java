package org.shah.solution.config;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.shah.solution.common.SMSGatewayResponseErrorHandler;
import org.shah.solution.config.GatewayConfig;
import org.shah.solution.model.SmsGatewayConfigObj;
import org.shah.solution.notification.model.Notification;
import org.shah.solution.repository.SmsGatewayRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
public class CommonConfig {

	private static Logger logger = LoggerFactory.getLogger(CommonConfig.class);

	@Autowired
	private SMSGatewayResponseErrorHandler errorHandler;
	
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		RestTemplate template = new RestTemplate();
		template.setRequestFactory(getClientHttpRequestFactory());
		template.setErrorHandler(errorHandler);
		return template;
	}
	
	@Bean
	public JAXBContext getMarshallerContext() throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(Notification.class);
		return jaxbContext;
	}

	@Bean
	public GatewayConfig loadSmsGatewayConfig(SmsGatewayRepository smsGateway) {
		List<SmsGatewayConfigObj> config = smsGateway.findAll();
		Map<String, SmsGatewayConfigObj> configMap = new HashMap<String, SmsGatewayConfigObj>();
		if (config != null) {
			for (Iterator<SmsGatewayConfigObj> iterator = config.iterator(); iterator.hasNext();) {
				SmsGatewayConfigObj smsGatewayConfigObj = (SmsGatewayConfigObj) iterator.next();
				configMap.put(smsGatewayConfigObj.getCountryCode(), smsGatewayConfigObj);
			}
			logger.info("SMS server configuration has Loaded..");
		} else {
			logger.error("Error Occured in loading SMS server Configuration");
		}

		return new GatewayConfig(configMap);
	}
	private ClientHttpRequestFactory getClientHttpRequestFactory() {
	    int timeout = 5000;
	    HttpComponentsClientHttpRequestFactory clientHttpRequestFactory
	      = new HttpComponentsClientHttpRequestFactory();
	    clientHttpRequestFactory.setConnectTimeout(timeout);
	    return clientHttpRequestFactory;
	}

}

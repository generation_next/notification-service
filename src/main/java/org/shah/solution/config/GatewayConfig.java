package org.shah.solution.config;

import java.util.Map;

import org.shah.solution.model.SmsGatewayConfigObj;

public class GatewayConfig {
	private Map<String, SmsGatewayConfigObj> gatewayMap;

	public GatewayConfig(Map<String, SmsGatewayConfigObj> gatewayMap) {
		this.gatewayMap = gatewayMap;
	}

	/**
	 * @return the gatewayMap
	 */
	public Map<String, SmsGatewayConfigObj> getGatewayMap() {
		return gatewayMap;
	}

	/**
	 * @param gatewayMap
	 *            the gatewayMap to set
	 */
	public void setGatewayMap(Map<String, SmsGatewayConfigObj> gatewayMap) {
		this.gatewayMap = gatewayMap;
	}

}

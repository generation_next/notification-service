package org.shah.solution.mq;
import javax.xml.bind.JAXBException;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLStreamException;
import org.shah.solution.notification.model.Notification;
import org.shah.solution.service.NotificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
 
@Component
public class JmsConsumer {
	
	@Autowired
	private NotificationService notificationService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(JmsConsumer.class);
	
	@JmsListener(destination = "${jsa.activemq.queue}", containerFactory="jsaFactory")
	public void receive(String msg) throws FactoryConfigurationError{
		LOGGER.debug("Recieved Message: " + msg);
		try {
		Notification notification = notificationService.unmarshal(msg);
		notificationService.sendMessage(notification);
		}catch(JAXBException | XMLStreamException e) {
		System.out.println(e.getMessage());	
		}
	}
}

package org.shah.solution.mq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ErrorHandler;

public class DefaultErrorHandler implements ErrorHandler{

private static final Logger LOG = LoggerFactory.getLogger(JmsConsumer.class);	
	
@Override
public void handleError(Throwable t) {
	LOG.warn("Error :");
	LOG.error(t.getMessage().toString());
	
}

}

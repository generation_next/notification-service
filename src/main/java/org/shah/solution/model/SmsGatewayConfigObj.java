package org.shah.solution.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "sms_gateway", catalog = "oauthdb", schema = "", uniqueConstraints = {
	    @UniqueConstraint(columnNames = {"countrycode"})})
public class SmsGatewayConfigObj implements Serializable{
	
	private static final long serialVersionUID = 2396993867858924114L;
	@Id
	@Column(name="countrycode")
	private String countryCode;
	@Column(name="device")
	private String device;
	@Column(name="apikey")
	private String apiKey;
	/**
	 * @return the countryCode
	 */
	public String getCountryCode() {
		return countryCode;
	}
	/**
	 * @param countryCode the countryCode to set
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * @return the device
	 */
	public String getDevice() {
		return device;
	}
	/**
	 * @param device the device to set
	 */
	public void setDevice(String device) {
		this.device = device;
	}
	
	public String getApiKey() {
		return apiKey;
	}
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	} 

}

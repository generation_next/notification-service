package org.shah.solution.repository;

import org.shah.solution.model.SmsGatewayConfigObj;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SmsGatewayRepository extends JpaRepository<SmsGatewayConfigObj, String> {
  
}

<html>
<body>
	<div style="margin: 0; padding: 0; background: #ffffff">
		<div style="" class="" has-hovered="true">
			<table id="x_backgroundTable"
				style="transform: scale(1, 1); transform-origin: left top 0px;"
				min-scale="0.5177195685670262" width="100%" cellpadding="0"
				border="0" cellspacing="0">
				<tbody>
					<tr>
						<td valign="top" align="center">
							<table class="" width="50%" cellpadding="0" border="0"
								align="center" cellspacing="0">
								<tbody>
									<tr>
										<td valign="top" align="left">
											<table class="x_devicewidth"
												style="border: 2px solid #4250f4" width="100%"
												cellpadding="0" border="0" cellspacing="0">
												<tbody>
													<tr>
														<td valign="top" align="left">
															<table width="80%" cellpadding="0" border="0"
																align="left" cellspacing="0">
																<tbody>
																	<tr>
																		<td><img src="cid:LOGO" alt="eSalus"
																			class="x_img_res" title="eSalus"
																			style="display: block; border: none; outline: none; text-decoration: none"
																			width="80;" height="70"></td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>
													<tr>
														<td bgcolor="#4250f4"><img src="cid:SPACER" width="2"
															height="20"></td>
													</tr>
													<tr>
														<td
															style="font-family: Arial; font-size: 22px; color: #ffffff; text-align: center; font-weight: bold; line-height: 24px"
															height="42" bgcolor="#4250f4">${sharedData.title}
															(<span data-markjs="true"
															style="background-color: yellow; color: black;">${data.OTP}</span>)!
														</td>
													</tr>
													<tr>
														<td bgcolor="#4250f4"><img src="cid:SPACER" width="2"
															height="20"></td>
													</tr>
													<tr>
													</tr>
													<tr>
														<td bgcolor="#ffffff"><img src="cid:SPACER" width="2"
															height="20"></td>
													</tr>
													<tr>
														<td valign="top" align="left">
															<table class="x_width" width="100%" cellpadding="0"
																border="0" cellspacing="0">
																<tbody>
																	<tr>
																		<td width="19" valign="top" align="left"></td>
																		<td width="540" valign="top" align="left">
																			<table class="x_width" width="604" cellpadding="0"
																				border="0" cellspacing="0">
																				<tbody>
																					<tr>
																						<td class="x_float" width="40%" valign="top"
																							align="left">
																							<table class="x_float" width="100%"
																								cellpadding="0" border="0" align="left"
																								cellspacing="0">
																								<tbody>
																									<tr>
																										<td><img src="cid:SPACER" width="2"
																											height="40"></td>
																									</tr>
																									<tr>
																										<td style="text-align: center" valign="top"
																											align="center"><img src="cid:OTPJPG"
																											alt="OTP" title="OTP" width="130" height="90"></td>
																									</tr>
																								</tbody>
																							</table>
																						</td>
																						<td class="x_float" width="100%" valign="middle"
																							align="center">
																							<table class="x_float" width="100%"
																								cellpadding="0" border="0" align="left"
																								cellspacing="0">
																								<tbody>
																									<tr>
																										<td
																											style="font-family: Arial; font-size: 14px; color: #818183; text-align: left; line-height: 16px">
																											Dear ${data.name}, <br> <br>
																											<p>
																												${sharedData.content}<strong
																													style="font-size: 16px; line-height: 18px; color: #b02b30">
																													${sharedData.body} (<span
																													data-markjs="true"
																													style="background-color: yellow; color: black;">${data.OTP}</span>)
																												</strong>
																											</p> <br> <br> Sincerely,<br>
																											Genex Techs Team, <br> Texas, Branch
																										</td>
																									</tr>
																								</tbody>
																							</table>
																						</td>
																					</tr>
																				</tbody>
																			</table>
																		</td>
																		<td width="19" valign="top" align="left"></td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>
													<tr>
														<td bgcolor="#ffffff"><img src="cid:SPACER" width="2"
															height="20"></td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
									
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
										<td valign="top" align="center">
											<table width="606" cellpadding="0" border="0" align="center"
												cellspacing="0">
												<tbody>

													<tr>
														<td valign="top" bgcolor="#4250f4" align="left">
														<td width="10px" valign="middle" align="center" bgcolor="#4250f4"><img
															src="cid:MAILTO" alt="mailto" title="mailto" width="22"
															height="23"></td>
														<td
															style="font-family: arial; font-size: 12px; color: #ffffff; background: #4250f4"
															width="95%" valign="middle" align="left"><a
															href="mailto:${data.mailfrom}" target="_blank"
															rel="noopener noreferrer"
															style="color: #ffffff; text-decoration: none; outline: none; border: none">support@esalus.com</a>
														</td>



													</tr>
												</tbody>
											</table>
										</td>
									</tr>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>
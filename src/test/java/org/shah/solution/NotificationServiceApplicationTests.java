package org.shah.solution;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.shah.solution.common.NotificationData;
import org.shah.solution.common.NotificationsContent;
import org.shah.solution.common.TemplateCodes;
import org.shah.solution.notification.model.Notification;
import org.shah.solution.service.SMSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class NotificationServiceApplicationTests {

	@Autowired
	TemplateCodes templateCodes;
	
	@Autowired
	NotificationsContent notificationsContent;
	
	@Autowired
	SMSService msgService;
	
	@Test
	public void contextLoads() {
	}

	@Test
	@Ignore
	public void propertyTestTemplateCodes() {
		Map<String, String> prop = templateCodes.getTemplate();
		assertEquals("Mail Code :", "registration", prop.get("001"));
	}
	
	@Test
	@Ignore
	public void propertyTestNotifications() {
		Map<String, NotificationData> notification = notificationsContent.getNotification();
		//assertEquals("Mail Code :", "registration", notification.get("registration"));
		NotificationData not = notification.get("registration");
		NotificationData reset = notification.get("reset");
		System.out.println(not);
		System.out.println(reset);
	}
	
	@Test
	@Ignore
	public void testServiceCall() throws UnsupportedOperationException, IOException {
		CloseableHttpClient httpClient = HttpClients.createDefault();
		//DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpPost postRequest = new HttpPost(
			"https://smsgateway.me/api/v4/message/send");

		StringEntity input = new StringEntity("[{\"phone_number\":18175017682,\"message\":\"iPad 4\",\"device_id\":\"91130\"}]");
		input.setContentType("application/json");
		postRequest.setEntity(input);
		postRequest.addHeader("Authorization", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhZG1pbiIsImlhdCI6MTUyNzEzMjM3MSwiZXhwIjo0MTAyNDQ0ODAwLCJ1aWQiOjUzNDUzLCJyb2xlcyI6WyJST0xFX1VTRVIiXX0.cPCTBr4qaB_PvBdre5gA83b3HhJuxJXAynjPOYr9xu8");
		CloseableHttpResponse response = httpClient.execute(postRequest);
		//HttpResponse response = httpClient.execute(postRequest);

		if (response.getStatusLine().getStatusCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatusLine().getStatusCode());
		}

		BufferedReader br = new BufferedReader(
                        new InputStreamReader((response.getEntity().getContent())));

		String output;
		System.out.println("Output from Server .... \n");
		while ((output = br.readLine()) != null) {
			System.out.println(output);
		}

		httpClient.close();
	}
	
	@Test
	@Ignore
	public void restCallTest() throws UnsupportedEncodingException {
		//ClientHttpRequestFactory requestFactory = getClientHttpRequestFactory();
		RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());
		//StringEntity input = new StringEntity("[{\"phone_number\":18175017682,\"message\":\"iPad 4\",\"device_id\":\"91130\"}]");
		//input.setContentType("application/json");
		HttpHeaders headers = new HttpHeaders();
		// can set the content Type
		headers.setContentType(MediaType.APPLICATION_JSON);

		//Can add token for the authorization
		String req = "[{\"device_id\":\"91130\",\"phone_number\":\"18175017682\",\"message\":\"IMPORTANT: Your verification code is :45335B. This code is valid only for next 15 minutes.\"}]";
		headers.add(HttpHeaders.AUTHORIZATION, "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhZG1pbiIsImlhdCI6MTUyNzEzMjM3MSwiZXhwIjo0MTAyNDQ0ODAwLCJ1aWQiOjUzNDUzLCJyb2xlcyI6WyJST0xFX1VTRVIiXX0.cPCTBr4qaB_PvBdre5gA83b3HhJuxJXAynjPOYr9xu8");	
		HttpEntity<String> request = new HttpEntity<>(req, headers);
		String result = restTemplate.postForObject("https://smsgateway.me/api/v4/message/send", request, String.class);
		System.out.println(result);
		
		
	}
	
	@Test
	public void testMessagingSMS() throws JsonProcessingException {
		Notification notification = new Notification();
		notification.setUserName("Tester");
		notification.setUserEmail("faisal.ahmd@gmail.com");
		notification.setUserCountry("US");
		notification.setUserCountryCode("1");
		notification.setUserPhone("8175017682");
		notification.setMessage("Test");
		notification.setMessageCode("002");
		notification.setMessageType("MOB");
		NotificationData data = new NotificationData();
		data.setContent("This is for Testing");
		data.setMessage("IMPORTANT: Your verification code is :$code$. This code is valid only for next 15 minutes.");
		data.setSubject("Account Activation Code");
		data.setTitle("Account Activation Code");
		msgService.sendSMSMsg(notification, data);
		
	}
	private ClientHttpRequestFactory getClientHttpRequestFactory() {
	    int timeout = 5000;
	    HttpComponentsClientHttpRequestFactory clientHttpRequestFactory
	      = new HttpComponentsClientHttpRequestFactory();
	    clientHttpRequestFactory.setConnectTimeout(timeout);
	    return clientHttpRequestFactory;
	}
}
